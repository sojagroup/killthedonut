

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.Timer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;


public class Board extends JPanel{

	Color designColor= Color.gray;
	Color bckgndColor= Color.white;
	Color alternColor= Color.darkGray;
	private boolean antialiasing = true;
	int speed = 5;
	int lastBrick = 0;
	int numOfPoints = 0;
	int level = 1;
	int showLevel = 1;
	int design = 1;
	int donutHits = 0;
	// GameState 
	//          0 = Intro
	//          1 = Menue
	//          2 = PreLevelScreen
	//          3 = Game
	//          4 = PostLevelScreen
	//          5 = Pause
	private enum GameState { INTRO, MENUE ,PRELVLSCRN ,GAME, POSTLVLSCRN, PAUSE };
	private GameState _gameState = GameState.INTRO;
	private final int maxNumDesigns = 15;
	private final int maxNumLevels = 10;
	   Random rand = new Random();
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private final int DELAY = 25/*25*/; // animation speed
	String message = "Game Over";
	String startMessage1 = "Kill The Donut!";
	String startMessage2 = "Once upon a time ... ah fuck, kill the Donut!";
	String startMessage3 = "Press button to start game!";
	String startMessage4 = "";
	String startMessage5 = "http://bitbucket.org/sojagroup/killthedonut";
	//boolean ingame = true;
	//private Image ball;
	private Timer timer;
	Paddle paddle;
	Ball ball;
	Donut donut;
	Brick bricks[];
	MovingBrick movingBrick;
	Points points;
	DonutLifeBar donutLifeBar;
	LevelBackground levelBackground;
	ArrayList <Sprite> objects;


	public Board() {
        timer = new Timer(DELAY, new MyTimerActionListener());
        timer.start();
		initBoard();
		//gameLoop();
	}

	private void initBoard() {

		setBackground(Color.darkGray);
		setPreferredSize(new Dimension(Commons.WIDTH, Commons.HEIGTH));
		setDoubleBuffered(true);

		//loadImage();
		
		// Keyboard-Abfrage
		addKeyListener(new TAdapter());
        setFocusable(true);
        
        // Objekte erzeugen
        levelBackground = new LevelBackground(350, 350);
        paddle = new Paddle(100, 20);
        ball = new Ball(20, 20);
        donut = new Donut(250, 250);
        bricks = new Brick[30];
        points = new Points();
        donutLifeBar = new DonutLifeBar();
        objects = new ArrayList<Sprite>();
        movingBrick = new MovingBrick(100, 130, 40, 20);
        for (int i = 0; i < 30; i++){
        	bricks[i] = new Brick(0,0,40,20);
        }

        resetObjects();
        setSpeed(5);
        _gameState = GameState.INTRO; // Intro
	}
	public void setHiGraphicsMode(boolean g)
	{
		antialiasing = g;
	}
	private void resetObjects(){
		donutHits= 0;
		objects.clear();
		paddle.resetState();
		ball.resetState();
        donut.resetState();
        donutLifeBar.resetState();
 
        //objects.add(levelBackground);
        objects.add(donut);
        objects.add(paddle);
        objects.add(ball);
        objects.add(donutLifeBar);
        
	    if(rand.nextInt(10)+1 > 5){
	    	int k = 0;
	    	for (int i = 0; i < 5; i++) {
	    		for (int j = 0; j < 6; j++) {
	    			bricks[k].resetState();
	    			bricks[k].setSize(j * 50 + 30, i * 20 + 20, 40, 20);
	    			objects.add(bricks[k]);
	    			k++;
	    		}
	    	}
	    	movingBrick.resetState();
	    	movingBrick.setSize(100, 130, 40, 20);
	    }
	    else{
	    	int k = 0;
	    	for (int i = 0; i < 5; i++) {
	    		for (int j = 0; j < 6; j++) {
	    			bricks[k].resetState();
	    			bricks[k].setSize(j * 50 + 30, i * 20 + 50, 40, 20);
	    			objects.add(bricks[k]);
	    			k++;
	    		}
	    	}
	    	movingBrick.resetState();
	    	movingBrick.setSize(100, 20, 40, 20);
	    }
	    objects.add(movingBrick);
	    if(level < maxNumLevels){   
	    	for (int i = 0; i < 30; i++) {
	    		if(rand.nextInt(10)+1 > 5){
	    			bricks[i].setDestroyed(true);
	    			bricks[i].setDestroyed(true);
	    		}
	    	}
	    }
	}

	// Neuzeichnen des Fensterinhalts, getriggert von repaint() in actionPerformed() (Timer)
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		// Objekte zeichnen
		drawObjects(g);
	}

	private void drawText(Graphics g, Color c, int size, int yOffset, String msg){
       	Font font = new Font("Verdana", Font.BOLD, size);
    	FontMetrics metr = this.getFontMetrics(font);

    	g.setColor(c);
    	g.setFont(font);

    	g.drawString(msg,
                 (Commons.WIDTH - metr.stringWidth(msg)) / 2,
                 Commons.WIDTH / 2+ yOffset);
	}
	private int count = 520;
	private boolean slide = false;

	public void drawObjects(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        // Rendering hints werden verwendet um "weiche" Linien zu erzeugen (Antialiasing). 
        if(antialiasing){
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
				RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        }
        //g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
       /* g2d.setRenderingHint( RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY );
        g2d.setRenderingHint( RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY );
        g2d.setRenderingHint( RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED );*/
    	// GameStates: 
    	//          0 = Intro
    	//          1 = Menue
    	//          2 = PreLevelScreen
    	//          3 = Game
    	//          4 = PostLevelScreen
    	//          5 = Pause
        switch(_gameState){
        case INTRO: /****************************************************/
        	    /*                      INTRO                       */
        	    /****************************************************/
        	
        	if(count > 0)
        		count--;
        	else
        	{
        		slide = !slide;
        		count = 520;
        	}
        	
        	if(slide)
        	{
        		//setDesign(1);//setBackground(Color.white);
        		drawText(g, Color.gray, (count-200)*-1, count+count-300, startMessage1);
        		drawText(g, designColor, 12, (int)(count*1.5)-500, startMessage2);
        		drawText(g, Color.gray, 12, count-300+50, startMessage3);
        		drawText(g, Color.gray, 12, 60, startMessage4);
        		drawText(g, Color.gray, 12, (int)(count*1.5)-300, startMessage5);
        		// Objekte zeichnen
        		donut.draw(g2d);
        		//for(Sprite obj : objects){
        		//	obj.draw(g2d);
        		//}
        	}
        	else
        	{
        		setDesign(4); //setBackground(Color.black);
        		drawText(g, Color.gray, 30, (int)(count*1.5)-500, "SojaGroup");
        		drawText(g, designColor, 30, count-300, "SoJaGroup");
        		drawText(g, designColor, 10, count-300+30, "proudly presents...");
        		drawText(g, Color.gray, 35, count+count-300+70, "Kill The Donut!");
        		drawText(g, designColor, 10, (int)(count*1.5)-300, "(a game by DiTo, Fette-Club & MiFri)");
        	}
        	break;
        case MENUE: /****************************************************/
    	        /*                      MENUE                       */
    	        /****************************************************/
        	
        	    /* not implemented yet */
        	break;
        case PRELVLSCRN: /****************************************************/
    	        /*                  PRELEVELSCREEN                  */
    	        /****************************************************/
			// Objekte zeichnen
    		for(Sprite obj : objects){
    			obj.draw(g2d);
    		}
			points.draw(g, "Points: "+ numOfPoints, speed, level /*showLevel*/);
        	break;
        case GAME: /****************************************************/
    	        /*                       GAME                       */
    	        /****************************************************/
			// Objekte zeichnen
    		for(Sprite obj : objects){
    			obj.draw(g2d);
    		}
			points.draw(g, "Points: "+ numOfPoints, speed, level/*showLevel*/);
        	break;
        case POSTLVLSCRN: /****************************************************/
    	        /*                  POSTLEVELSCREEN                 */
    	        /****************************************************/
        	if(level!=1 && level < maxNumLevels){
        		showLevel = level;
            	drawText(g, designColor, 20, 0, "Level: " + level);
        	}
        	else{
            	drawText(g, designColor, 20, 0, message);
        	}
			points.draw(g, "Points: "+ (numOfPoints), speed/*, /*level/*showLevel*/);
        	break;
        case PAUSE: /****************************************************/
    	        /*                      PAUSE                       */
    	        /****************************************************/
			// Objekte zeichnen
        	showLevel = level;
        	drawText(g, designColor, 20, 0, "Pause");
    		for(Sprite obj : objects){
    			obj.draw(g2d);
    		}
        	break;
        }
		Toolkit.getDefaultToolkit().sync();
	}
    private class TAdapter extends KeyAdapter {

        @Override
		public void keyReleased(KeyEvent e) {
            paddle.keyReleased(e);
    		int key = e.getKeyCode();
    		switch(_gameState){
    		case INTRO: // Intro
        			//ball.setBallInGame();
    				setDesign(1);
        			_gameState = GameState.PRELVLSCRN; // PreLevelScreen
        			count = 520;
        			slide = false;

    			break;
    		case MENUE: // Menue
    			break;
    		case PRELVLSCRN: //PreLevelScreen
            	//if(!ball.getBallInGame()){
        		if (key == KeyEvent.VK_UP) {
        			ball.setBallInGame();
        			_gameState = GameState.GAME; // Game

        		}
            	//}
    			break;
    		case GAME: // Game
            	if(!ball.getBallInGame()){
        		if (key == KeyEvent.VK_UP) {
        			ball.setBallInGame();
        			//_gameState = 3; // Game
        		}
            	}
        		if(key == KeyEvent.VK_P) {
        				_gameState = GameState.PAUSE; // Pause
        		}

    			break;
    		case POSTLVLSCRN: // PostLevelScreen
    			if(level > 1 && level < maxNumLevels){
    				ball.setBallInGame();
    				_gameState = GameState.PRELVLSCRN; // PreLevelScreen (Next level)
    			}
    			else{
    				level = 1;
    				_gameState = GameState.INTRO;
    			}
    			startNewLevel();
    			break;
    		case PAUSE: // Pause
    			_gameState = GameState.GAME; // Game
    			break;
    		}
        }

        @Override
		public void keyPressed(KeyEvent e) {
        	if(_gameState == GameState.GAME || _gameState == GameState.PRELVLSCRN)
            paddle.keyPressed(e);

        }
    }
 /*   public void update(){
        // Paddle Bewegung
      	switch(_gameState){
      	case INTRO: // Intro
      		// Bewegung 
      		if(count == 0)
      		{
      			setDesign(1);
      		for(Sprite obj : objects){
      			obj.move();
      		}
      		}
      		break;
      	case MENUE: // Menue
      		break;
      	case PRELVLSCRN: // PreLevelScreen
  			ball.setX(paddle.getX()+40);
  			ball.setY(paddle.getY()-20);
      		// Bewegung 
      		for(Sprite obj : objects){
      			obj.move();
      		}
      		break;
      	case GAME: // Game
          	if(donut.getFinalAnimEnds()){
          		if((level)!= maxNumLevels){
          			message = "Completed Level: " + (level-1)+"/"+maxNumLevels;
          		}
          		else{
                  	message = "Victory";
                  	//level = 1;
          		}
     			_gameState =GameState.POSTLVLSCRN; // PostLevelScreen
          	}
          	else{
          		// Bewegung 
          		for(Sprite obj : objects){
          			obj.move();
          		}
          		checkCollision();
          	}
      		break;
      	case POSTLVLSCRN: // PostLevelScreen
      		break;
      	}
        repaint();  
    }*/
/*	public void gameLoop()
	{
	   long lastLoopTime = System.nanoTime();
	   final int TARGET_FPS = 60;
	   final long OPTIMAL_TIME = 1000000000 / TARGET_FPS;   
	   int lastFpsTime = 0;
	   int fps =0;

	   // keep looping round til the game ends
	   while (true)
	   {
	      // work out how long its been since the last update, this
	      // will be used to calculate how far the entities should
	      // move this loop
	      long now = System.nanoTime();
	      long updateLength = now - lastLoopTime;
	      lastLoopTime = now;
	      double delta = updateLength / ((double)OPTIMAL_TIME);

	      // update the frame counter
	      lastFpsTime += updateLength;
	      fps++;
	      
	      // update our FPS counter if a second has passed since
	      // we last recorded
	      if (lastFpsTime >= 1000000000)
	      {
	         System.out.println("(FPS: "+fps+")");
	         lastFpsTime = 0;
	         fps = 0;
	      }
	      update();
	      //GUI.board.dr
	      // update the game logic
	      //doGameUpdates(delta);
	      
	      // draw everyting
	      //render();
	      
	      // we want each frame to take 10 milliseconds, to do this
	      // we've recorded when we started the frame. We add 10 milliseconds
	      // to this and then factor in the current time to give 
	      // us our final value to wait for
	      // remember this is in ms, whereas our lastLoopTime etc. vars are in ns.
	      try {
			Thread.sleep( (lastLoopTime-System.nanoTime() + OPTIMAL_TIME)/1000000 );
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   }
	}*/
    class MyTimerActionListener implements ActionListener {
  	  public void actionPerformed(ActionEvent e) {

          // Paddle Bewegung
       	switch(_gameState){
       	case INTRO: // Intro
       		//if(count ==0)
       			//setDesign(1);
       		// Bewegung 
       		for(Sprite obj : objects){
       			obj.move();
       		}
       		break;
       	case MENUE: // Menue
       		break;
       	case PRELVLSCRN: // PreLevelScreen
   			ball.setX(paddle.getX()+40);
   			ball.setY(paddle.getY()-20);
       		// Bewegung 
       		for(Sprite obj : objects){
       			obj.move();
       		}
       		break;
       	case GAME: // Game
           	if(donut.getFinalAnimEnds()){
           		if(level< maxNumLevels){
               	message = "Completed Level: " + (level-1)+"/"+maxNumLevels;
           		}
           		else{
                   	message = "Victory";
                   	//level = 1;
           		}
      			_gameState =GameState.POSTLVLSCRN; // PostLevelScreen
           	}
           	else{
           		// Bewegung 
           		for(Sprite obj : objects){
           			obj.move();
           		}
           		checkCollision();
           	}
       		break;
       	case POSTLVLSCRN: // PostLevelScreen
       		break;
       	}
           repaint();
       

  	  }

  	}

    public void stopGame() {
        _gameState = GameState.POSTLVLSCRN;
        //timer.cancel();
    }
    public void startNewLevel() {

    	resetObjects();

        setSpeed(speed);

        if(level == 1|| level > maxNumLevels){
            setDesign(1);
            numOfPoints = 0;
            showLevel = 1;
            level = 1;
        }
        else{
        	int temp = rand.nextInt(maxNumDesigns)+1;
        	if(design == temp)
        		temp++;
        	if (temp > maxNumDesigns)
        		temp = 1;
        		
        	setDesign(temp);
        }
    }
    public void setSpeed(int speed) {
    	this.speed = speed;
    		ball.setSpeed(speed);
    		if(speed>4)
    			paddle.setSpeed(3);
 /*   		switch(speed){ // sp 2 = p 2, sp 3 = p 2, sp 4 = p 2, sp 5 = p 2, sp 6 = p 3
    		case 5:
    			paddle.setSpeed(3);
    			break;
    		case 6:
    			paddle.setSpeed(3);
    			break;
    		default:
    			paddle.setSpeed(2);
    		}*/

    }
    public void setDesign(int design){
    	if(this.design != design)
    	{
    	switch (design){
    	case 1:
    		designColor = Color.gray; bckgndColor = Color.white; alternColor = Color.darkGray;
    		break;
    	case 2:
    		designColor = Color.lightGray; bckgndColor = Color.darkGray; alternColor = Color.gray;
    		break;
    	case 3:
    		designColor = Color.darkGray; bckgndColor = Color.lightGray; alternColor = Color.gray;
    		break;
    	case 4:
    		designColor = Color.orange; bckgndColor = Color.darkGray; alternColor = Color.yellow;
    		break;
    	case 5:
    		designColor = Color.magenta; bckgndColor = Color.black; alternColor = Color.green;
    		break;
    	case 6:
    		designColor = Color.green; bckgndColor = Color.darkGray; alternColor = Color.cyan;
    		break;
    	case 7:
    		designColor = Color.cyan; bckgndColor = Color.darkGray; alternColor = Color.blue;
    		break;
    	case 8:
    		designColor = Color.blue; bckgndColor = Color.orange; alternColor = Color.darkGray;
    		break;
    	case 9:
    		designColor = Color.green; bckgndColor = Color.black; alternColor = Color.orange;
    		break;
    	case 10:
    		designColor = Color.lightGray; bckgndColor = Color.black; alternColor = Color.cyan;
    		break;
    	case 11:
    		designColor = Color.black; bckgndColor = Color.orange; alternColor = Color.darkGray;
    		break;
    	case 12:
    		designColor = Color.yellow; bckgndColor = Color.blue; alternColor = Color.darkGray;
    		break;
    	case 13:
    		designColor = Color.darkGray; bckgndColor = Color.cyan; alternColor = Color.gray;
    		break;
    	case 14:
    		designColor = Color.magenta; bckgndColor = Color.white; alternColor = Color.gray;
    		break;
    	case 15:
    		designColor = Color.pink; bckgndColor = Color.darkGray; alternColor = Color.orange;
    		break;
    	}
        // Farben zwischenspeichern
    	this.design = design;
    	
    	// Farben setzen
		setBackground(bckgndColor);
		points.setColorScheme(designColor, bckgndColor, alternColor);
		for(Sprite obj : objects){
			obj.setColorScheme(designColor, bckgndColor, alternColor);
		}
        repaint();
    	}
    }
    public void checkCollision() {
    	if(_gameState == GameState.GAME){ // Game started?
    	if(ball.getY() > 305){
    		paddle.setShieldActivated();
    	}
    	if(paddle.getShieldActivated()){
    		if(ball.getY() > 310 && ball.getY() < 317)
    			ball.setYDir(-1);
    		
    	}
    	else if (ball.getY() > Commons.BOTTOM) {
    		//if(movingBrick.isDestroyed())
    			//setSpeed(speed-2);
            message = "Game Over";
            level = 1;
            stopGame();
        }
    	
    	if(movingBrick.isDestroyed()&& !donut.getIsInHittingAnim()&& !donut.isDestroyed()){
        	for (int i = 0; i < 30; i++) {
        		if(bricks[i].getRect().intersects(donut.getRespawnBox())&& bricks[i].isDestroyed()&&donut.getYDir()== 1){
        			bricks[i].setActive();
        			donut.setYDir(-1);
        		}
        	}
    	}
    	if(movingBrick.isDestroyable()&& donut.getY() > 120 && donut.getYDir()== 1)
    		donut.setYDir(-1);
    
    	if((ball.getRect()).intersects(donut.getHitBox())){
    		if(!donut.getIsInHittingAnim()){ // donut ist nicht in Hit-Animation
    			if(donut.getYDir()==-1)
    				donut.setYDir(1);
    			else
    				donut.setYDir(-1);
    			if(donut.getXDir()==-1)
    				donut.setXDir(1);
    			else
    				donut.setXDir(-1);
    			
    			if(movingBrick.isDestroyed()){
    				donutHits++;
    				donutLifeBar.setHit();
    				if(donutHits >= 3){
						for (int i = 0; i < 30; i++) {
							if(!bricks[i].isDestroyed()){
								bricks[i].setDestroyed(true);
								bricks[i].setDestroyed(true);
							}
						}	
						donut.explosion.start(2);
						donut.setFinalAnim();
						ball.setBallOffGame();
						//setSpeed(speed-2);
						numOfPoints += (150*speed*level);
						level++;
						//showLevel++;
    				}
    			}
    			donut.setHittingAnim();
    			if(!movingBrick.isDestroyable()){
    	        /*for (int i = 0; i < 30; i++) {
    	        	if(bricks[i].isDestroyed()){
    	        		bricks[i].setActive();
    	        		break;
    	        	}
    	        }*/
    				
    				if(bricks[lastBrick].isDestroyed()){
    					bricks[lastBrick].setActive();
    					bricks[lastBrick].points.start(101*speed*level, 2);
    					// falls der letzte zerst�rrte  Brick wieder aktiviert wurde
    					// muss ein neuer letzter Brick zum wiederherstellen er-
    					// mittelt werden. Brick-Array durchlaufen bis ein anderer
    					// zerst�rrter Brick gefunden wird.
    					if(rand.nextInt(10)+1 > 5){
    						for (int i = 0; i < 30; i++) {
    							if(bricks[i].isDestroyed()){
    								lastBrick=i;
    								break;
    							}
    						}
    					}
    					else{
    						for (int i = 29; i > -1; i--) {
    							if(bricks[i].isDestroyed()){
    								lastBrick=i;
    								break;
    							}
    						}
    					}
    	    	        numOfPoints -= (101*speed*level);
    				}
    	        }
    		}
    	}
    	else
    	{
    		if(!movingBrick.isDestroyed()){
    			if(!donut.getIsInHittingAnim())
    				donut.setColor(designColor);
    			else
    				donut.setColor(Color.red);
    		}
    		else
        		donut.setColor(Color.red);
    	}
    	if ((ball.getRect()).intersects(paddle.getRect())) {

            int paddleLPos = (int)paddle.getRect().getMinX();
            //int ballLPos = (int)ball.getRect().getMinX();
            int ballLPos = ball.getCenterX();


            int first = paddleLPos + 20/*8*/;
            int second = paddleLPos + 40/*16*/;
            int third = paddleLPos + 60/*24*/;
            int fourth = paddleLPos + 80/*32*/;

            if (ballLPos < first) {
                ball.setXDir(-1);
                ball.setYDir(-1);
            }

            if (ballLPos >= first && ballLPos < second) {
                ball.setXDir(-1);
            	//if(ball.getXDir()==0)
            	//	ball.setXDir(-1);
                ball.setYDir(-1 /* ball.getYDir()*/);
            }

            if (ballLPos >= second && ballLPos < third) {
                ball.setXDir(0);
                ball.setYDir(-1);
            }

            if (ballLPos >= third && ballLPos < fourth) {
            	ball.setXDir(1);
            	//if(ball.getXDir()==0)
            	//	ball.setXDir(1);
            	ball.setYDir(-1 /* ball.getYDir()*/);
            }

            if (ballLPos >= fourth) {
            	ball.setXDir(1);
            	ball.setYDir(-1);
            }
        }
    	for (int i = 0, j = 0; i < 30; i++) {
            if (bricks[i].isDestroyed()) {
                j++;
                if (j == 30) {
                    movingBrick.setIsDestroyable(true);
                	//System.out.println(j);
                }
            }
        }

    	for (int i = 0; i < 30; i++) {
            if ((ball.getRect()).intersects(bricks[i].getRect())) {

                int ballLeft = (int)ball.getRect().getMinX();
                int ballHeight = (int)ball.getRect().getHeight();
                int ballWidth = (int)ball.getRect().getWidth();
                int ballTop = (int)ball.getRect().getMinY();

                Point pointRight =
                    new Point(ballLeft + ballWidth + 1, ballTop);
                Point pointLeft = new Point(ballLeft - 1, ballTop);
                Point pointTop = new Point(ballLeft, ballTop - 1);
                Point pointBottom =
                    new Point(ballLeft, ballTop + ballHeight + 1);

                if (!bricks[i].isDestroyed()) {
                    if (bricks[i].getRect().contains(pointRight)) {
                        ball.setXDir(-1);
                    }

                    else if (bricks[i].getRect().contains(pointLeft)) {
                        ball.setXDir(1);
                    }

                    if (bricks[i].getRect().contains(pointTop)) {
                        ball.setYDir(1);
                    }

                    else if (bricks[i].getRect().contains(pointBottom)) {
                        ball.setYDir(-1);
                    }

                    bricks[i].setDestroyed(true);
                    if(bricks[i].isDestroyed()){
                    	lastBrick = i;
                    	if(!movingBrick.isDestroyed()){
                    	numOfPoints+=(100*speed*level);
                    	bricks[i].points.start(100*speed*level, 1);
                    	}
                    	else
                    		bricks[i].points.start("---", 1);
                    	paddle.setBrickDestroyed();
                    }
                    //ball.increaseSpeed();
                }
            }
    	}
        if ((ball.getRect()).intersects(movingBrick.getRect())) {

            int ballLeft = (int)ball.getRect().getMinX();
            int ballHeight = (int)ball.getRect().getHeight();
            int ballWidth = (int)ball.getRect().getWidth();
            int ballTop = (int)ball.getRect().getMinY();

            Point pointRight =
                new Point(ballLeft + ballWidth + 1, ballTop);
            Point pointLeft = new Point(ballLeft - 1, ballTop);
            Point pointTop = new Point(ballLeft, ballTop - 1);
            Point pointBottom =
                new Point(ballLeft, ballTop + ballHeight + 1);

            if (!movingBrick.isDestroyed()) {
                if (movingBrick.getRect().contains(pointRight)) {
                    ball.setXDir(-1);
                }

                else if (movingBrick.getRect().contains(pointLeft)) {
                    ball.setXDir(1);
                }

                if (movingBrick.getRect().contains(pointTop)) {
                    ball.setYDir(1);
                }

                else if (movingBrick.getRect().contains(pointBottom)) {
                    ball.setYDir(-1);
                }

                if(movingBrick.isDestroyable){
                	movingBrick.setDestroyed(true);
                	if(movingBrick.isDestroyed()){
                		//setSpeed(speed+2);
                    	donutLifeBar.setActive();
                		numOfPoints += (150*speed);
                		movingBrick.points.start(150*speed*level, 1);
                	}
                	
                }
            }
        }
    }
    }
}


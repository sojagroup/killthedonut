import javax.swing.SwingUtilities;

public class KillTheDonut {
	GuiTest GUI;
	// Standard-Constructor
	public KillTheDonut(){

		// GUI-Zeugs
		GUI = new GuiTest();
		GUI.createWindow("Kill The Donut");
		//GUI.MessageBox(os + "Hallo Welt - Java sucks!!!\n" + bla1 + "\n" + bla2, null);
	}

	/**
	* @param args
	*/
	public static void main(String[] args) {
		// Swing ist nicht Thread-Safe, um nicht  später Probleme zu bekommen,
		// sollte Swing-Komponente erzeugender Code immer im Event-Dispatcher-
		// Thread (EDT) ausgeführt werden. Daher wird das ganze Programm auf den EDT
		// geschmissen. Die main-Funktion bleibt im Programm-Thread, da ja static.
		// Und das soll einfacher sein als 'ne richtige Programmiersprache?
		// Java ist scheiße!
		
        // Verpacke den auszuführenden Programmcode in ein eigenes
        // Runnable-Objekt, um diesen nachher im Event Dispatching
        // Thread ausführen zu können
		System.out.println("Starte main-Funktion:  " + Thread.currentThread());
        Runnable starterProgramm = new Runnable() {
        	@Override
			public void run() {
        		new KillTheDonut(); // Programm-Instanz auf EDT-Thread erzeugen
        	}
        };
    	// Führe den obigen Quellcode im Event-Dispatch-Thread aus
        SwingUtilities.invokeLater(starterProgramm);

	}	
}



import java.awt.Color;
import java.awt.Graphics2D;

public class Explosion extends Sprite {
	
	private boolean _startAnimations = false;
	private boolean _animationFinish = false;
	private int     _animationLength = 25;
	private int     _zoomDelta = 0;
	private int     _animationType = 1;
	   
    public Explosion(int x, int y, int breite, int hoehe) {
        this.x = x;
        this.y = y;
        width = breite;
        heigth = hoehe;
    }
    public void draw(Graphics2D g2d){
    
    	if(_startAnimations){
    		// Farbe
    		g2d.setColor(_farbe3);
        
    		switch(_animationType){
    		case 1:
        		g2d.drawArc(x-_zoomDelta, y-_zoomDelta, 
        				    width+(_zoomDelta*2), heigth+(_zoomDelta*2),0, 360);
    			break;
    		default:
        		g2d.setColor(Color.red);
        		g2d.drawArc(x-_zoomDelta, y-_zoomDelta, 
        				    width+(_zoomDelta*2), heigth+(_zoomDelta*2),0, 360);	
    		}
    	}
    }
    @Override
    public void move()
    {
    	if(_startAnimations){
    		if(_animationLength > 0){
    			_animationLength -= 1;
    			_zoomDelta += 1;
    		}
    		else{
    		 _animationFinish = true;
    		 _startAnimations = false;
    		}
    	}
    }
    public void start(int type){
    	this._animationType = type;
    	resetState();
    	_startAnimations = true;
    }
    public boolean getHitAnimEnds(){
    	return _animationFinish;
    }
    public void resetState() 
    {
      _zoomDelta = 0;
      _animationLength = 25;
      _animationFinish = false;
    }
}



import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GraphicsConfigTemplate;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Donut extends Sprite implements Commons{
	
	   private int xdir;
	   private int ydir;
	   private int zoom;
	   private boolean zoomOut = true;
	   private Color standardColor = Color.gray;
	   private boolean hitingAnimation = false;
	   private boolean finalAnimation = false;
	   private boolean finalAnimEnds = false;
	   private int HitAnimCount = 100;
	   private BufferedImage offImg;
	   private BufferedImage offImg2;
	    boolean destroyed = false;
	   private int wi;
	   AffineTransform at = new AffineTransform();
	   Random rand = new Random();
	   Explosion explosion;
	   public Donut(int Breite, int Hoehe) {

	     xdir = -1;
	     ydir = -1;

	     width = 20;
	     heigth = 20;

	     wi = Breite;
	     setHitBox(-15,-15,30, 30);
	     resetState();
	     explosion = new Explosion(-15, -15, 30, 30);
	     
	     
		     offImg2 = new BufferedImage(Breite, Hoehe, BufferedImage.TYPE_INT_ARGB);
		     Graphics2D g2 = (Graphics2D)offImg2.getGraphics();
		     //g.setColor(Color.white); 
		     //g.fillRect(0, 0, Breite, Hoehe);
		     g2.setColor(Color.red);
		    
				g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		        g2.setRenderingHint( RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY );
		        g2.setRenderingHint( RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY );
		        g2.setRenderingHint( RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED );
		        // Donut zeichnen
		        // Erzeugen der Ellipse 
		        Ellipse2D e2 = new Ellipse2D.Double(0, 0, wi/3, wi/3);
		        //g2d.setStroke(new BasicStroke(1));

		        // Ellipse 72 mal rotiert zeichnen, um Donut-Form zu erhalten
		        for (double deg = 0; deg < 360; deg += 8) {
		            AffineTransform at
		                    = AffineTransform.getTranslateInstance(Breite/2, Hoehe/2);
		            at.rotate(Math.toRadians(deg));
		            g2.draw(at.createTransformedShape(e2));
		        }
		        g2.dispose();
	    }
	   @Override
	    public void setColorScheme(Color farbe1, Color farbe2, Color farbe3){
		   super.setColorScheme(farbe1, farbe2, farbe3);
	    	createDonut();
	    	
	    }
	   public void createDonut(){
		    //GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		    //GraphicsDevice gs = ge.getDefaultScreenDevice();
		    //GraphicsConfigTemplate gcr = null;
		   
		    //GraphicsConfiguration gc = gs.getDefaultConfiguration();
		    
		     //Test
		     offImg = new BufferedImage(wi, wi, BufferedImage.TYPE_INT_ARGB);
		     //offImg = gc.createCompatibleImage(wi, wi, Transparency.BITMASK);

		     Graphics2D g = (Graphics2D)offImg.getGraphics();
		     //g.setColor(Color.white); 
		     //g.fillRect(0, 0, Breite, Hoehe);
		     g.setColor(farbe);
		    
				g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		        g.setRenderingHint( RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY );
		        g.setRenderingHint( RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY );
		        g.setRenderingHint( RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED );
		        // Donut zeichnen
		        // Erzeugen der Ellipse 
		        Ellipse2D e = new Ellipse2D.Double(0, 0, wi/3, wi/3);
		        //g2d.setStroke(new BasicStroke(1));

		        // Ellipse 72 mal rotiert zeichnen, um Donut-Form zu erhalten
		        for (double deg = 0; deg < 360; deg += 8) {
		            AffineTransform at
		                    = AffineTransform.getTranslateInstance(wi/2, wi/2);
		            at.rotate(Math.toRadians(deg));
		            g.draw(at.createTransformedShape(e));
		        }
		        g.dispose();
	   }


	    public void move()
	    {
	    	if(!finalAnimation){
	      x += xdir;
	      y += ydir;

	      if (x <= 0) {
	        setXDir(1);
	      }

	      if (x >= BALL_RIGHT) {
	        setXDir(-1);
	      }

	      if (y <= 0) {
	        setYDir(1);
	      }
	      
	      if (y >= 250) {
	    	  setYDir(-1);
	      }
	    	}
	      if(!hitingAnimation){
	    	  // Animation
	    	  if(zoomOut){
	    		  zoom += 1;
	    	  }
	    	  else{
	    		  zoom -= 1;
	    	  }
	    	  if(zoom > 5){
	    		  zoomOut = false;
	    	  }
	    	  else if(zoom < -5){
	    		  zoomOut = true;
	    	  }
	      }
	      else{
	    	  if(HitAnimCount > 0){
	    		  HitAnimCount -= 1;
	    		  if(!finalAnimation && HitAnimCount < 50){
	          	zoom += 1;
	    		  }
	    		  else
	    			  zoom -=1;
	    	  }
	    	  else{
	    		  HitAnimCount = 100;
	    		  hitingAnimation = false;
	    		  zoom = 0;
		    	  if(finalAnimation){
		    		  finalAnimEnds = true;
		    		  destroyed = true;
		    	  }
	    	  }
	    	  
	      }
	      explosion.move();
	    }
	    public void setHittingAnim(){
	    	if(HitAnimCount==100){
	    	  hitingAnimation = true;
	    	}
	    }
	    public void setFinalAnim(){
	    	if(HitAnimCount==100){
	    	  hitingAnimation = true;
	    	}
	    	finalAnimation = true;
	    }
	    public boolean getFinalAnimEnds(){
	    	return finalAnimEnds;
	    }
	    public boolean getIsInHittingAnim(){
	    	return hitingAnimation;
	    }
	    public void draw(Graphics2D g2d){
	    	if(!destroyed){
	    		if(farbe == Color.red)
	    			g2d.drawImage(offImg2, x-wi/2-zoom, y-wi/2-zoom, wi+zoom*2, wi+zoom*2, null);
	    		else
	    			g2d.drawImage(offImg, x-wi/2-zoom, y-wi/2-zoom, wi+zoom*2, wi+zoom*2, null);
	    		explosion.setX(x-15);
	    		explosion.setY(y-15);
	    		explosion.draw(g2d);
	    	}
	    	/*if(!destroyed){
	    	// Farbe
	        g2d.setColor(farbe);
			
	        // Donut zeichnen
	        // Erzeugen der Ellipse 
	        Ellipse2D e = new Ellipse2D.Double(0, 0, wi/5+zoom, wi/3+zoom);
	        //g2d.setStroke(new BasicStroke(1));

	        // Ellipse 72 mal rotiert zeichnen, um Donut-Form zu erhalten
	        for (double deg = 0; deg < 360; deg += 8) {
	            AffineTransform at
	                    = AffineTransform.getTranslateInstance(x, y);
	            at.rotate(Math.toRadians(deg));
	            g2d.draw(at.createTransformedShape(e));
	        }
	        if(drawHitBox)
	        {
	        g2d.setColor(Color.red);
            g2d.drawRect(x+hx,y+hy,hw,hh);
	        }
	        explosion.setX(x-15);
	        explosion.setY(y-15);
	        explosion.draw(g2d);
	    	}*/
	    }
	    public void setDestroyed(boolean destroyed)
	    {
	    		this.destroyed = destroyed;
	    		//explosion.start(1);
	    }
	    public boolean isDestroyed()
	    {
	    		return this.destroyed;
	    		//explosion.start(1);
	    }
	    public Rectangle getRespawnBox(){
	    	return new Rectangle(x-1, y-1, 2, 2);
	    }

	    public void resetState() 
	    {
	      x = rand.nextInt(320);
	      y = rand.nextInt(200);
	      hitingAnimation = false;
		  finalAnimation = false;
		  finalAnimEnds = false;
		  HitAnimCount = 100;
		  destroyed = false;
		  //createDonut();
		  farbe = standardColor;
		  zoomOut = true;

	    }
	    public void resetColor(){
	    	farbe = standardColor;
	    }
	    public void setColor(Color farbe){
	    	this.farbe = farbe;
	    }
	    public void setXDir(int x)
	    {
	      xdir = x;
	    }

	    public void setYDir(int y)
	    {
	      ydir = y;
	    }

	    public int getYDir()
	    {
	      return ydir;
	    }
	    public int getXDir()
	    {
	      return xdir;
	    }
}




import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;


public class Paddle extends Sprite implements Commons {
	
	int dx;
	int speed=2;
	private int brickHitCount = 0;
	private int maxBrickHits = 5;
	private boolean shieldActivated = false;
	private int shieldTime = 25;
	private int shieldCount = 0;
	   Explosion explosion;
    public Paddle(int breite, int hoehe) {

        width = breite;
        heigth = hoehe;

        resetState();
        explosion = new Explosion(x, y, breite, hoehe);
    }

    public void move() {
        x += (dx*speed);
        if (x <= 2) 
          x = 2;
        if (x >= Commons.PADDLE_RIGHT)
          x = Commons.PADDLE_RIGHT;
        
        if(shieldCount > 0)
        	shieldCount--;
        explosion.setX(x);
        explosion.setY(y);
		explosion.move();

    }
    
    public void draw(Graphics2D g2d){
    	
    	// Farbe
        if(shieldCount > 0)
            g2d.setColor(_farbe3);
        else
        g2d.setColor(farbe);
        
        // Paddle zeichnen
        g2d.drawRoundRect(x, y, width, heigth,10, 10);
        
        // Design
        g2d.fillRoundRect(x+5, y+5, width-9, heigth-9,5, 5);
        g2d.setColor(_farbe2);
        g2d.fillRoundRect(x+8, y+8, width-15, heigth-15,10, 10);

        g2d.setColor(farbe);
        switch(brickHitCount){
        case 1:
            g2d.fillRect(x+8, y+9, width-75, heigth-17);
            //g2d.fillRect(x+8, y+9, width-75, heigth-17);
            break; 
        case 2:
            g2d.fillRect(x+8, y+9, width-60, heigth-17);
            break;
        case 3:
            g2d.fillRect(x+8, y+9, width-45, heigth-17);
            break;
        case 4:
            g2d.fillRect(x+8, y+9, width-31, heigth-17);
            break;
        case 5:
            g2d.fillRect(x+8, y+9, width-15, heigth-17);
            break;
         default:
        }
        if(shieldCount > 0){
        	g2d.drawRect(0, 330, 350, 7);
        	g2d.fillRect(0, 333, 350, 2);
        }
        explosion.draw(g2d);
    }
    @Override
    public void setColorScheme(Color farbe1, Color farbe2, Color farbe3){
    	super.setColorScheme(farbe1, farbe2, farbe3);
    	explosion.setColorScheme(farbe1, farbe2, farbe3);
    	
    }
    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT) {
            dx = -2;

        }

        if (key == KeyEvent.VK_RIGHT) {
            dx = 2;
        }
        
        if (key == KeyEvent.VK_DOWN) {
        	if(brickHitCount==5){
            brickHitCount =0;
            shieldActivated = true;
            shieldCount = shieldTime;
        	}
        }
    }

    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT) {
            dx = 0;
        }

        if (key == KeyEvent.VK_RIGHT) {
            dx = 0;
        }
    }
    public void setBrickDestroyed(){
    	if(brickHitCount < maxBrickHits){
    	brickHitCount++;
    	if(brickHitCount==5){
    		explosion.setX(x);
    		explosion.setY(y);
    		explosion.start(1);
    	}
    	
    	}
    }
    public void setShieldActivated(){
    	if(brickHitCount==5){
            brickHitCount =0;
            shieldActivated = true;
            shieldCount = shieldTime;
            explosion.start(1);
        	}
    	
    }
    public boolean getShieldActivated(){
    	return (shieldCount > 0);
    }
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void resetState() {
        x = 130;
        y = 300;
        brickHitCount = 0;
        shieldCount = 0;
        shieldActivated = false;
    }

}

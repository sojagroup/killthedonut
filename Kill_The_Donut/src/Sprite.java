
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class Sprite {
	
	// Sprite position and size variables
    protected int x, y, width, heigth;
    // HitBox coordinates and size
    protected int hx, hy, hw, hh;
    // colors
    protected Color farbe = Color.gray;
    protected Color _farbe1 = Color.gray;
    protected Color _farbe2 = Color.white;
    protected Color _farbe3 = Color.darkGray;
    protected boolean drawHitBox = false;
    
    /**
     * This method sets the position and size for the object
     * @param x - x coordinate
     * @param y - y coordinate
     * @param w - width (in relation to x) 
     * @param h - height (in relation to y)
     */
    public void setSize(int x, int y, int w, int h){
    	this.x = x;
    	this.y = y;
    	this.width = w;
    	this.heigth = h;
    }
    /*
     *  Getter and Setter
     */
    public void setX(int x) { this.x = x; }
    public void setY(int y) { this.y = y; }
    public int getX() { return x; }
    public int getY() { return y; }
    public int getWidth() { return width; }
    public int getHeight() { return heigth; }
    
    /*
     * Object size and hitbox as Rectangle
     */
    Rectangle getRect(){ return new Rectangle(x, y, width, heigth); }
    Rectangle getHitBox(){ return new Rectangle(x+hx, y+hy, hw, hh); }

    /**
     * This method sets the hitting box for the object
     * @param hx - x coordinate
     * @param hy - y coordinate
     * @param hw - width (in relation to x) 
     * @param hh - height (in relation to y)
     */
    public void setHitBox(int hx, int hy, int hw, int hh){
    	this.hx = hx;
    	this.hy = hy;
    	this.hw = hw;
    	this.hh = hh;
    }
    /**
     * This method has to be implemented by child class. Use it for
     * code to draw the object.
     * @param g2d
     * Graphics2D object
     */
    public void draw(Graphics2D g2d){
    	/* this method has to be implemented by child class */
    }
    /**
     * This method has to be implemented by child class. Use it for
     * code to move the object.
     */
    public void move() {
    	/* this method has to be implemented by child class */
    }
    /**
     * Set color scheme of object. There are three values predefined 
     * you can use for coloring.
     * @param farbe1 - first color (main color)
     * @param farbe2 - second color
     * @param farbe3 - third color
     */
    public void setColorScheme(Color farbe1, Color farbe2, Color farbe3){
    	this.farbe= farbe1;
    	this._farbe1 = farbe1;
    	this._farbe2 = farbe2;
    	this._farbe3 = farbe3;
    	
    }
    public void setZweitFarbe(Color farbe){ this._farbe2 = farbe; }
    public void setDrittFarbe(Color farbe){ this._farbe3 = farbe; }
    public Color getColor(){
    	return this.farbe;
    }
    public Color getZweitFarbe(){ return this._farbe2; }
    public Color getDrittFarbe(){ return this._farbe3; }
}

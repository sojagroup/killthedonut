import java.awt.Color;
import java.awt.Graphics2D;

public class DonutLifeBar extends Sprite {
	
	private boolean _active;
	private int _hits;
	
	public DonutLifeBar(){
		_active = false;
		_hits = 0;
	}
	public void resetState(){
		_active = false;
		_hits = 0;
	}
    public void draw(Graphics2D g2d){
    	
        //explosion.draw(g2d);
        //points.draw(g2d);
    	// Farbe
        // Donut Leben Anzeige
    	g2d.setColor(farbe);
        g2d.drawArc(10, 10, 10, 10, 0, 360);
        g2d.drawArc(25, 10, 10, 10, 0, 360);
        g2d.drawArc(40, 10, 10, 10, 0, 360);
        if(_active){
        	g2d.setColor(Color.red);
        	switch(_hits){
        	case 0:
				g2d.fillArc(10+1, 10+1, 9, 9, 0, 360);
				g2d.fillArc(25+1, 10+1, 9, 9, 0, 360);
				g2d.fillArc(40+1, 10+1, 9, 9, 0, 360);
        		break;
        	case 1:
				g2d.fillArc(10+1, 10+1, 9, 9, 0, 360);
				g2d.fillArc(25+1, 10+1, 9, 9, 0, 360);
        		break;
        	case 2:
				g2d.fillArc(10+1, 10+1, 9, 9, 0, 360);
        		break;
        	}
        }
    }
    public void setActive(){
    	_active = true;
    }
    public void setHit(){
    	if(_hits < 3)
    		_hits++;
    }
}

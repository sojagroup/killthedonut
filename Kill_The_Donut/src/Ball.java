

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Ball extends Sprite implements Commons {
	
	   private int xdir;
	   private int ydir;
	   private int speed = 4;
	   private boolean ballIsInGame = false;
	   private boolean removeBall = false;
	   Random rand = new Random();
	   private BufferedImage offImg;
	   public Ball(int Breite, int Hoehe) {
	     xdir = rand.nextInt(2)-1;
	     ydir = -1;

	     width = Breite;
	     heigth = Hoehe;

	     resetState();
	    	createBall();
	    }

	    public void setSpeed(int speed) {
	        this.speed = speed;
	    }
	    public void increaseSpeed() {
	        this.speed +=1;
	    }
		   public void createBall(){
			    //GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			    //GraphicsDevice gs = ge.getDefaultScreenDevice();
			    //GraphicsConfigTemplate gcr = null;
			   
			    //GraphicsConfiguration gc = gs.getDefaultConfiguration();
			    offImg = null;
			     //Test
			     offImg = new BufferedImage(width+1, heigth+1, BufferedImage.TYPE_INT_ARGB);
			     //offImg = gc.createCompatibleImage(wi, wi, Transparency.BITMASK);

			     Graphics2D g = (Graphics2D)offImg.getGraphics();
			     //g.setColor(Color.white); 
			     //g.fillRect(0, 0, Breite, Hoehe);
			     g.setColor(farbe);
			    
					g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
					//g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
			        //g.setRenderingHint( RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY );
			        //g.setRenderingHint( RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY );
			        //g.setRenderingHint( RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED );
			        //Ball zeichnen
			        g.drawArc(0, 0, width, heigth, 0, 360);
			        g.fillArc(3, 3, width-5, heigth-5, 0, 360);
			        g.dispose();
		   }
	    public void move()
	    {
	    	if(ballIsInGame && (!removeBall)){
	      x += (xdir*speed);
	      y += (ydir*speed);

	      if (x <= 0) {
	        setXDir(1);
	      }

	      if (x >= BALL_RIGHT) {
	        setXDir(-1);
	      }

	      if (y <= 0) {
	        setYDir(1);
	      }
	      }
	    }
	    public int getCenterX(){
	    	return width/2+x;
	    }
	    public void setBallInGame(){
	    	if(!ballIsInGame){
	    		ballIsInGame = true;
	    		removeBall=false;
	    	}
	    }
	    public void setBallOffGame(){
	    	resetState();
    		removeBall = true;
	    }
	    public boolean getBallInGame(){
	    		return ballIsInGame;
	    }
	    public void draw(Graphics2D g2d){
	    	if(!removeBall){
	    		
	    		// Farbe
	    		g2d.setColor(farbe);
	    		if(Commons.USE_IMAGE_RENDERING){
	    			g2d.drawImage(offImg, x, y, width+1, heigth+1, null);
	    		}
	    		else{        
	    			//Ball zeichnen
	    			g2d.drawArc(x, y, width, heigth, 0, 360);
	    			g2d.fillArc(x+3, y+3, width-5, heigth-5, 0, 360);
	    		}
	    	}
	    }

	    public void resetState() 
	    {
	    	ballIsInGame = false;
	 	  removeBall = false;
	    	
		     if(rand.nextInt(10)+1 > 5)
		    	 xdir = 1;
		     else
		    	 xdir = -1;


		     ydir = -1;
	      x = 170;
	      y = 280;
	    }
	    @Override
	    public void setColorScheme(Color farbe1, Color farbe2, Color farbe3){
	    	super.setColorScheme(farbe1, farbe2, farbe3);
	    	createBall();
	    }
	    public void setXDir(int x)
	    {
	      xdir = x;
	    }

	    public void setYDir(int y)
	    {
	      ydir = y;
	    }

	    public int getYDir()
	    {
	      return ydir;
	    }
	    public int getXDir()
	    {
	      return xdir;
	    }

}


import java.awt.Color;

//Import-Anweisung f�r JFrame-, JDialog-, JButton-, JLabel-Klassen usw, 
import javax.swing.*;


public class GuiTest {
	final Board board= new Board();
	// mal ein Fenster erzeugen
	public void createWindow(String title){
		// Erzeugung eines neuen Frames mit Titel
		final JFrame window = new JFrame();
		// Fenstertitel
		window.setTitle(title);
		// Fenstergroesse
		//window.setSize(640, 420);
		window.setBackground(Color.darkGray);
		
		// Hack um Frame zentriert auszugeben
		window.setLocationRelativeTo(null);
		// Gr��en�nderung durch User unterbinden
		window.setResizable(false);
		
		
		//final JLabel background=new JLabel(new ImageIcon("res/breakout2.png"));


		//board = new Board();
		// simplen Text-String einf�gen 
		//window.add(new JLabel("Hallo Welt"));
		JMenu menu = new JMenu("Menue");

		JMenuItem itemStart = new JMenuItem("New Game");
		menu.add(itemStart);
        itemStart.addActionListener(new java.awt.event.ActionListener(){
            // Beim Dr�cken des OK-Buttons wird actionPerformed aufgerufen
            @Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
                // Dialog schlie�en   
            	board.level = 1;
            	board.startNewLevel();
            }
        } );
		JMenuItem itemMsg = new JMenuItem("Test");
		//menu.add(itemMsg);
        itemMsg.addActionListener(new java.awt.event.ActionListener(){
            // Beim Dr�cken des OK-Buttons wird actionPerformed aufgerufen
            @Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
                // Dialog schlie�en   
            	MessageBox("Hello World", "Men�");
            }
        } );
		JMenuItem itemInfo = new JMenuItem("Info");
		//menu.add(itemInfo);
        itemInfo.addActionListener(new java.awt.event.ActionListener(){
            // Beim Dr�cken des OK-Buttons wird actionPerformed aufgerufen
            @Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
            	String t = "Men� wurde geklickt\n\n"
            			  +"Dies ist ein simples Test-Programm, um ein wenig\n"
            			  +"damit rumzuspielen. Es erf�llt keinen weiteren\n"
            			  +"Zweck. Daher ist dieser Text auch v�llig sinnfrei.\n\n"
            			  +"Achja, dies ist eine stupide MessageBox wie man sie\n"
            			  +"aus Windows kennt und sollte jetzt so halbwegs\n"
            			  +"funktionieren. Gruss, Micha.\n\n"
            			  +"Entwickler: \n\nH. v. Pirch, D. Scholz, M. Friedrich\n"
            			  +"2015";

                // Display Message   
            	MessageBox(t, "Info");
            }
        } );

		JMenuItem itemClose = new JMenuItem("Quit Game");
		menu.add(itemClose);
        itemClose.addActionListener(new java.awt.event.ActionListener(){
            // Beim Dr�cken des OK-Buttons wird actionPerformed aufgerufen
            @Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
                // Programm beenden   
            	System.exit(0);
            }
        } );
		JMenu menuBall = new JMenu("Skill Level");
		JMenuItem itemMsgSlow = new JMenuItem("I'm too young to die");
		JMenuItem itemMsgSlox = new JMenuItem("Hey, not to rough");
		JMenuItem itemMsgNorm = new JMenuItem("Hurt me plenty");
		JMenuItem itemMsgFast = new JMenuItem("Ultra-Violence");
		JMenuItem itemMsgMega = new JMenuItem("Nightmare!");
		menuBall.add(itemMsgSlow);
		menuBall.add(itemMsgSlox);
		menuBall.add(itemMsgNorm);
		menuBall.add(itemMsgFast);
		menuBall.add(itemMsgMega);
        itemMsgSlow.addActionListener(new java.awt.event.ActionListener(){
            // Beim Dr�cken des OK-Buttons wird actionPerformed aufgerufen
            @Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
                // Programm beenden   
            	board.setSpeed(2);
            }
        } );
        itemMsgSlox.addActionListener(new java.awt.event.ActionListener(){
            // Beim Dr�cken des OK-Buttons wird actionPerformed aufgerufen
            @Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
                // Programm beenden   
            	board.setSpeed(3);
            }
        } );
        itemMsgNorm.addActionListener(new java.awt.event.ActionListener(){
            // Beim Dr�cken des OK-Buttons wird actionPerformed aufgerufen
            @Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
                // Programm beenden   
            	board.setSpeed(4);
            }
        } );
        itemMsgFast.addActionListener(new java.awt.event.ActionListener(){
            // Beim Dr�cken des OK-Buttons wird actionPerformed aufgerufen
            @Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
                // Programm beenden   
            	board.setSpeed(5);
            }
        } );
        itemMsgMega.addActionListener(new java.awt.event.ActionListener(){
            // Beim Dr�cken des OK-Buttons wird actionPerformed aufgerufen
            @Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
                // Programm beenden   
            	board.setSpeed(6);
            }
        } );
        JMenu menuGraphics = new JMenu("Graphics");
        JMenuItem iGraphicsLo = new JMenuItem("High Performance");
        JMenuItem iGraphicsHi = new JMenuItem("High Quality");
        menuGraphics.add(iGraphicsLo);
        menuGraphics.add(iGraphicsHi);
        iGraphicsLo.addActionListener(new java.awt.event.ActionListener(){
            // Beim Dr�cken des OK-Buttons wird actionPerformed aufgerufen
            public void actionPerformed(java.awt.event.ActionEvent e) {
                // Farben 
            	board.setHiGraphicsMode(false);
            }
        } );
        iGraphicsHi.addActionListener(new java.awt.event.ActionListener(){
            // Beim Dr�cken des OK-Buttons wird actionPerformed aufgerufen
            public void actionPerformed(java.awt.event.ActionEvent e) {
                // Farben 
            	board.setHiGraphicsMode(true);
            }
        } );
/*		JMenu menuDesgn = new JMenu("Theme");
		JMenuItem itemMsg01 = new JMenuItem("Standard");
		JMenuItem itemMsg02 = new JMenuItem("Dark Future");
		JMenuItem itemMsg03 = new JMenuItem("Liquid Crystal Display");
		JMenuItem itemMsg04 = new JMenuItem("Golden Light");
		JMenuItem itemMsg05 = new JMenuItem("Crazy Trip");
		menuDesgn.add(itemMsg01);
		menuDesgn.add(itemMsg02);
		menuDesgn.add(itemMsg03);
		menuDesgn.add(itemMsg04);
		menuDesgn.add(itemMsg05);
		
		itemMsg01.addActionListener(new java.awt.event.ActionListener(){
            // Beim Dr�cken des OK-Buttons wird actionPerformed aufgerufen
            public void actionPerformed(java.awt.event.ActionEvent e) {
                // Farben 
            	board.setDesign(1);
            }
        } );
		itemMsg02.addActionListener(new java.awt.event.ActionListener(){
            // Beim Dr�cken des OK-Buttons wird actionPerformed aufgerufen
            public void actionPerformed(java.awt.event.ActionEvent e) {
                // Farben  
            	board.setDesign(2);
            }
        } );
		itemMsg03.addActionListener(new java.awt.event.ActionListener(){
            // Beim Dr�cken des OK-Buttons wird actionPerformed aufgerufen
            public void actionPerformed(java.awt.event.ActionEvent e) {
                // Farben  
            	board.setDesign(3);
            }
        } );
		itemMsg04.addActionListener(new java.awt.event.ActionListener(){
            // Beim Dr�cken des OK-Buttons wird actionPerformed aufgerufen
            public void actionPerformed(java.awt.event.ActionEvent e) {
                // Programm beenden   
            	board.setDesign(4);
            }
        } );
		itemMsg05.addActionListener(new java.awt.event.ActionListener(){
            // Beim Dr�cken des OK-Buttons wird actionPerformed aufgerufen
            public void actionPerformed(java.awt.event.ActionEvent e) {
                // Programm beenden   
            	board.setDesign(5);
            }
        } );*/

		
		JMenuBar bar = new JMenuBar();
		//javax.swing.Boarder bo = new javax.swing.border.LineBoarder(java.awt.Color.yellow);
		//bar.setBoarder(bo);
		bar.add(menu);
		bar.add(menuBall);
		bar.add(menuGraphics);
		//bar.add(menuDesgn);
		window.setJMenuBar(bar);
		window.add(board);
		//window.setResizable(false);


		//window.add(background);

        window.pack();
		// Fenster sichtbar
		window.setVisible(true);
		// Das Ding hier ist wichtig, Java setzt standardm��ig JFrame.HIDE_ON_CLOSE, somit liefe die Anwendung
		// ohne sichtbares Fenster weiter
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	/**
	 * Displays a modal dialog box that contains a button, 
	 * and a brief application-specific message, such as status or error information. The message box returns 
	 * an integer value that indicates which button the user clicked.
	 * @param sText - The message to be displayed. If the string consists of more than one line, 
	 * you can separate the lines using a carriage return and/or linefeed character between each line.
	 * @param sCaption - The dialog box title. If this parameter is null, the default title is Error.
	 */// unter Windows lieben gelernt!
	public int MessageBox(String sText, String sCaption){
		// some vars
		int buttonWidth = 100;
		int buttonHeight = 30;
		int distanceToFrame = 15; // sets the distance between content and dialog frame (0...35 maybe)
		int distTextToButton = 15; // distance between text pane and button (5 ... 30 maybe)
		int returnValue = 0; // not implemented yet
        // Erzeugung eines neuen Dialogs (final ist hier ein Hack wegen dem ActionListener())
        final JDialog dialog = new JDialog();
        // Titel wird gesetzt
        if(sCaption == null){
        	sCaption = "Error";
        }
        dialog.setTitle(sCaption);
        
        //JPanel surface = new JPanel();
        // da Java scheinbar nur auf GUI-Builder ausgelegt ist, m�ssen wir explizit angeben
        // kein verfickten Layout-Manager verwenden zu wollen, sonst lassen sich Elemente
        // in einem Fenster nicht frei positionieren!
        dialog.setLayout(null);
        
        // verhindern das Gr��e ver�ndert werden kann
        dialog.setResizable(false);
        
        // muss hier aufgerufen werden, sonst kommt man nicht an die Context-Gr��e
        dialog.pack();
        
        JTextPane text = new JTextPane();
        text.setFont(dialog.getFont());
        java.awt.FontMetrics metrics = text.getFontMetrics(text.getFont());
        
        // getting text height of selected Font
        int hgt = metrics.getHeight();
        // getting text width 
        int adv = metrics.stringWidth(sText);

        // The following is an absolutely horrorbile hack to check for carriage return (\n) in string
        // to determine height of text. In addition the longest text line is determined 
        // (for setting appropriate dialog width).
        int count = 0;
        String a = "";
        String b = "";
        for (char c : sText.toCharArray()) {
        	a += Character.toString(c);
            if (c == '\n') {
            	if(metrics.stringWidth(a) > metrics.stringWidth(b)){
            		b = a; // b contains the largest string
            	}
            	a=""; // empty string
               ++count;
            }           
        }
        if(count > 0){
        	hgt *= (count+1);
        	if(metrics.stringWidth(a) > metrics.stringWidth(b)){
        		b = a;       		
        	}
        	adv = metrics.stringWidth(b);
        }
        
        
        
        // ensures the dialog has a minimal width
        if(adv < 100){
        	adv = 100;
        }

        // width of dialog consists of text + spaceholder between content and frame + window decorations
        int dialogWidth = adv+distanceToFrame*2+dialog.getInsets().left + dialog.getInsets().right;
        // height of dialog consists of text + space between content and frame + window decorations + text/button distance
        int dialogHeight = hgt + distanceToFrame*2 + buttonHeight + dialog.getInsets().top + dialog.getInsets().bottom +distTextToButton;
        // Breite und H�he des Fensters setzen
        //surface.setSize(dialogWidth, dialogHeight);
        dialog.setSize(dialogWidth, dialogHeight);
        
        // Hack um Dialog zentriert auszugeben
        dialog.setLocationRelativeTo(null);
        
        // Text auf Dialog via JTextPane
        text.setText(sText/* Integer.toString(count)*/);
        text.setBounds(distanceToFrame,distanceToFrame, adv+20, hgt);
        text.setEditable(false);
        text.setBackground(null);
        dialog.add(text);

        
        //OK-Button auf Dialog
        JButton button = new JButton("OK");
        // Button Position und Gr��e
        button.setBounds(dialogWidth/2-buttonWidth/2-2, hgt+distanceToFrame+distTextToButton, buttonWidth, buttonHeight);
        // Das ist die Art und Weise wie Java Events zu handeln pflegt
        button.addActionListener(new java.awt.event.ActionListener(){
            // Beim Dr�cken des OK-Buttons wird actionPerformed aufgerufen
            @Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
                // Dialog schlie�en   
            	dialog.dispose();
            }
        } );
        dialog.add(button);
        //dialog.add(surface);

        // Dialog wird auf modal gesetzt
        dialog.setModal(true);
        
        // Wir lassen unseren Dialog anzeigen
        dialog.setVisible(true);
        
        return returnValue;    
	}
}

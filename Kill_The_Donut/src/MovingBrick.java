

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class MovingBrick extends Sprite implements Commons {

	boolean destroyed;
    boolean marked = false;;
	   private boolean doAnimation = true;
	   boolean isDestroyable;
	   private int zoom;
	   private boolean zoomOut = true;
	   private int xdir;
	   private int ydir;
	   private int initX;
	   private int initY;
	   Explosion explosion;
	   FallingPoints points;
		
    public MovingBrick(int x, int y, int breite, int hoehe) {
        this.x = initX = x;
        this.y = initY = y;
        
	     xdir = 1;
	     ydir = 0;

        width = breite;
        heigth = hoehe;

        destroyed = false;
        marked = false;
        isDestroyable = false;
        explosion = new Explosion(x, y, breite, hoehe);
        points = new FallingPoints(x,y);
      }
    @Override
    public void setColorScheme(Color farbe1, Color farbe2, Color farbe3){
    	super.setColorScheme(farbe1, farbe2, farbe3);
    	explosion.setColorScheme(farbe1, farbe2, farbe3);
    	
    }
    Rectangle getRect(){ return new Rectangle(x, y, width, heigth); }
    public void move()
    {
      x += (xdir);
      y += (ydir);

      if (x <= 30) {
        setXDir(1);
      }

      if (x >= BALL_RIGHT-60) {
        setXDir(-1);
      }
      if(marked)
      {
    	if(doAnimation){
    	      // Animation
    	      if(zoomOut)
    	      {
          	zoom += 1;
    	      }
    	      else
    	      {
          	zoom -= 1;
    	      }
    	      if(zoom > 1){
          	zoomOut = false;
    	      }
    	      else if(zoom < -1){
          	zoomOut = true;
    	      }
    	      width += (zoom*2);
    	      x += (zoom*-1);
    	      //heigth += zoom;
    	      doAnimation =false;
        	}
        	else
        		doAnimation = true;
      }
      explosion.move();
      points.move();
    }
    public void draw(Graphics2D g2d){
    	
    	// Farbe
        g2d.setColor(farbe);
        
        if (!isDestroyed()){
            g2d.drawRoundRect(x, y, width, heigth,10, 10);
            g2d.fillRoundRect(x+4, y+4, width-7, heigth-7,5, 5);
            g2d.setColor(_farbe2);
            g2d.fillArc(x+6, y+6, width-11, heigth-11, 0, 360);
        }
        explosion.draw(g2d);
        points.draw(g2d);
    }
    public void setXDir(int x)
    {
      xdir = x;
    }
      public boolean isDestroyed()
      {
        return destroyed;
      }
      public boolean isDestroyable()
      {
        return isDestroyable;
      }


      public void setDestroyed(boolean destroyed)
      {
    	if(marked){
    		this.destroyed = destroyed;
    		explosion.setX(x);
    		explosion.setY(y);
    		points.setX(x);
    		points.setY(y);
    		explosion.start(1);
    	}
    	else{
    		marked=true;
    		farbe = _farbe3;
    	}
      }
      public void setIsDestroyable(boolean destroyable)
      {
        this.isDestroyable = destroyable;
      }
	    public void resetState() 
	    {
		     xdir = 1;
		     ydir = 0;
		     x = initX;
		     y = initY;
		     this.destroyed = false;
		     marked = false;
		     zoom = 0;
		     isDestroyable = false;
		     zoomOut = true;
		     doAnimation = true;
	    }

}

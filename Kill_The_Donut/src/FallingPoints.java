

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

public class FallingPoints extends Sprite{
	private Font font;
	private boolean startAnimation = false;
	private int animCount = 25;
	private int ydir;
	private int initX;
	private int initY;
	private int points;
	private String txt; 
	private boolean useText = false;
	   private int type = 1;
	public FallingPoints(int x, int y) {
		font = new Font("Verdana", Font.BOLD, 12);
        this.x = initX = x;
        this.y = initY = y+10;
        ydir = 1;
		     //resetState();
		    }
    public void move()
    {
    	if(startAnimation){
    		if(animCount>0){
    		initY += ydir;
    		animCount--;
    		}
    		else
    			startAnimation=false;
    	}
    	

    }
	 public void draw(Graphics2D g2d){
		 if(startAnimation){
			 if(!useText){
				 switch(type){
				 case 1:
					 g2d.setColor(farbe);
					 g2d.setFont(font);
					 g2d.drawString("+"+points, x, initY);
					 break;
				 default:
					 g2d.setColor(Color.red);
					 g2d.setFont(font);
					 g2d.drawString("-"+points, x, initY);
				 }
			 }
			 else{
				 switch(type){
				 case 1:
					 g2d.setColor(farbe);
					 g2d.setFont(font);
					 g2d.drawString(txt, x, initY);
					 break;
				 default:
					 g2d.setColor(Color.red);
					 g2d.setFont(font);
					 g2d.drawString(txt, x, initY);
				 } 
			 }
		 }

	  }	 
	    public void start(int points, int type){
	    	useText = false;
	    	this.points = points;
	    	this.type = type;
	    	animCount = 25;
	    	//this.x = initX;
	    	initY = this.y +10;
	    	startAnimation = true;
	    }
	    public void start(String txt, int type){
	    	useText = true;
	    	this.txt = txt;
	    	this.type = type;
	    	animCount = 25;
	    	//this.x = initX;
	    	initY = this.y +10;
	    	startAnimation = true;
	    }

}


public interface Commons {
    public static final int WIDTH = 350;
    public static final int HEIGTH = 360;
    public static final int BOTTOM = 390;
    public static final int PADDLE_RIGHT = 250;
    public static final int BALL_RIGHT = 330;
    public static final boolean USE_IMAGE_RENDERING = true;
}

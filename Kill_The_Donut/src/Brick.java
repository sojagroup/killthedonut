

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;

public class Brick extends Sprite {

    boolean destroyed;
    boolean marked;
	   private int zoom;
	   private boolean zoomOut = true;
	   private boolean doAnimation = true;
	   Explosion explosion;
	   FallingPoints points;
	   private BufferedImage offImg;

    public Brick(int x, int y, int breite, int hoehe) {
      this.x = x;
      this.y = y;

      width = breite;
      heigth = hoehe;
      explosion = new Explosion(x, y, breite, hoehe);
      points = new FallingPoints(x,y);
      
  	createBrick();
      destroyed = false;
    }
    @Override
    public void setSize(int x, int y, int w, int h){
    	super.setSize(x, y, w, h);
    	explosion.setSize(x, y, w, h);
    	points.setSize(x, y, w, h);
    }
    public void resetState(){
        destroyed = false;
        this.marked=false;
        zoomOut = true;
 	   doAnimation = true;
 	   zoom = 0;
    }
	   public void createBrick(){
		    //GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		    //GraphicsDevice gs = ge.getDefaultScreenDevice();
		    //GraphicsConfigTemplate gcr = null;
		   
		    //GraphicsConfiguration gc = gs.getDefaultConfiguration();
		    offImg = null;
		     //Test
		     offImg = new BufferedImage(width+1, heigth+1, BufferedImage.TYPE_INT_ARGB);
		     //offImg = gc.createCompatibleImage(wi, wi, Transparency.BITMASK);

		     Graphics2D g = (Graphics2D)offImg.getGraphics();
		     //g.setColor(Color.white); 
		     //g.fillRect(0, 0, Breite, Hoehe);
		     g.setColor(farbe);
		    
				g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				//g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		        //g.setRenderingHint( RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY );
		        //g.setRenderingHint( RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY );
		        //g.setRenderingHint( RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED );
		        // Donut zeichnen
		        // Erzeugen der Ellipse 
	            g.drawRoundRect(0, 0, width, heigth,10, 10);
	            g.fillRoundRect(4, 4, width-7, heigth-7,5, 5);
	            g.setColor(_farbe2);
	            g.fillArc(6, 6, width-11, heigth-11, 0, 360);
		        g.dispose();
	   }
    @Override
    public void move()
    {
    	if(doAnimation){
    		// Animation
    		if(zoomOut) { zoom += 1; }
    		else { zoom -= 1; }
    		if(zoom > 1){ zoomOut = false; }
    		else if(zoom < -1){ zoomOut = true; }
	      
    		width += (zoom*2);
    		x -= zoom;
    		doAnimation =false;
    	}
    	else
    		doAnimation = true;
    	
    	explosion.move();
    	points.move();
    }
    @Override
    public void draw(Graphics2D g2d){
    	
        //explosion.draw(g2d);
        //points.draw(g2d);
    	// Farbe
        g2d.setColor(farbe);
        if (!isDestroyed()){
        	if(Commons.USE_IMAGE_RENDERING){
        		g2d.drawImage(offImg, x, y, width+1, heigth+1, null);
        	}
        	else{
        		g2d.drawRoundRect(x, y, width, heigth,10, 10);
        		g2d.fillRoundRect(x+4, y+4, width-7, heigth-7,5, 5);
        		g2d.setColor(_farbe2);
        		g2d.fillArc(x+6, y+6, width-11, heigth-11, 0, 360);
        		//explosion.draw(g2d);
        	}
        }
        else{
          	/*if(!explosion.getHitAnimEnds()){
        		explosion.draw(g2d);
        	}*/
        }
        explosion.draw(g2d);
        points.draw(g2d);
    }
    @Override
    public void setColorScheme(Color farbe1, Color farbe2, Color farbe3){
    	super.setColorScheme(farbe1, farbe2, farbe3);
    	explosion.setColorScheme(farbe1, farbe2, farbe3);
    	points.setColorScheme(farbe1, farbe2, farbe3);	
    	createBrick();
    }
    public int getZoom(){
    	return zoom;
    }
    public boolean isDestroyed()
    {
      return destroyed;
    }
    public void setActive()
    {
    	this.marked=false;
    	this.destroyed=false;
		farbe = _farbe1;
		_farbe2 = Color.red;
		createBrick();
		//explosion.resetState();
		explosion.start(2);
    }

    public void setDestroyed(boolean destroyed)
    {
    	if(marked){
    		this.destroyed = destroyed;
    		explosion.start(1);
 
    	}
    	else{
    		marked=true;
    		farbe = _farbe3;
    		createBrick();
    	}
    }
}
